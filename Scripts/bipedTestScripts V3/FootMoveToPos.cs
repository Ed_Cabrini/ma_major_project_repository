using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class FootMoveToPos : MonoBehaviour
{
    // Foot Solver Targets
    public GameObject rightFT;
    public GameObject leftFT;

    // Foot Target Pos Markers
    public GameObject rightFTPM;
    public GameObject leftFTPM;

    // Player Rig Root
    public GameObject playerRt;

    // Distance to from FT to FTPM
    private float rightDist;
    private float leftDist;

    // Pace length, foot move speed, foot lift limit
    public float stepDist;
    public float footSpd;
    public float footLiftDist;

    // DotTween Variables
    Tween rightFootMove;
    Tween leftFootMove;

    private void Update()
    {
        // Check distance between FTPM & FT
        if (rightFTPM != null && rightFT)
        {
            rightDist = Vector2.Distance(rightFTPM.transform.position, rightFT.transform.position); // Right foot distance
        }
        if (leftFTPM != null && leftFT)
        {
            leftDist = Vector2.Distance(leftFTPM.transform.position, leftFT.transform.position); // Left foot distance
        }
       
        // Move feet to new step location
        {
          if (rightDist > stepDist && !leftFootMove.IsActive())
          {
                if (!rightFootMove.IsActive())
                    rightFootMove = rightFT.transform.DOJump(rightFTPM.transform.position, footLiftDist, 1, footSpd);
          }
          if (leftDist > stepDist && !rightFootMove.IsActive())
          {
                if (!leftFootMove.IsActive())
                    leftFootMove = leftFT.transform.DOJump(leftFTPM.transform.position, footLiftDist, 1, footSpd);
          }
        }
    }
}