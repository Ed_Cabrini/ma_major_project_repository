using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerMethod2Simplified : MonoBehaviour
{
    // Movement variables
    public float movementSpeed;
    public float jumpStrength;

    private float movement;
    private bool isGrounded;
    public Rigidbody2D rb;
    private PlayerInputManager controls;

    // Object variables
    public Transform characterObject;
    public GameObject playerAvatar;

    // Get player rigidbody, assign new input, get movement context from input manager
    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        controls = new PlayerInputManager();
        controls.Player.Movement.performed += ctx => movement = ctx.ReadValue<float>();
        controls.Player.Movement.canceled += _ => movement = 0;

        controls.Player.Jump.started += _ => Jump();
    }

    private void OnEnable() => controls.Enable(); // Apply input
    private void OnDisable() => controls.Disable(); // Cancel input

    private void Update()
    {
        // Flip player based on movement direction
        if (movement < 0)
            characterObject.localScale = new Vector3(-1, characterObject.localScale.y, 1);
        else if (movement > 0)
            characterObject.localScale = new Vector3(1, characterObject.localScale.y, 1);
    }

    // Move player left & Right
    private void FixedUpdate()
    {
        rb.velocity = new Vector2(movement * movementSpeed, rb.velocity.y);
    }

    // Player jump logic
    private void Jump()
    {
        if (isGrounded)
        {
            rb.velocity = new Vector2(rb.velocity.x, jumpStrength);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            isGrounded = true;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            isGrounded = false;
        }
    }
}
