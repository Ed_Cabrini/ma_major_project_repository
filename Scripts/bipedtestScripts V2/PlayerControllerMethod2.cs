using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerMethod2 : MonoBehaviour
{
    // Movement variables
    public float movementSpeed;
    public float jumpStrength;

    private float movement;
    private bool isGrounded;
    public Rigidbody2D rb;
    private PlayerInputManager controls;

    // Object variables
    public Transform bumber;
    public Transform character;
    public GameObject playerAvatar;

    // Animation variables
    public Animator anim;

    // Get animation controller
    private void Start()
    {
        anim = playerAvatar.GetComponent<Animator>();
    }


    // Get player rigidbody, assign new input, get movement context from input manager
    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        controls = new PlayerInputManager();
        controls.Player.Movement.performed += ctx => movement = ctx.ReadValue<float>();
        controls.Player.Movement.canceled += _ => movement = 0;

        controls.Player.Jump.started += _ => Jump();
    }

    private void OnEnable() => controls.Enable(); // Apply input
    private void OnDisable() => controls.Disable(); // Cancel input

    private void Update()
    {
        // Rotate bumber based on player velocity
        bumber.rotation = Quaternion.LookRotation(rb.transform.forward, rb.velocity);

        // Flip player based on movement direction
        if (movement < 0)
            character.localScale = new Vector3(-5, 5, 5);
        else if (movement > 0)
            character.localScale = new Vector3(5, 5, 1);

        // Update anim parameters
        float h = rb.velocity.y;
        float v = rb.velocity.x;
        bool idle = false;

        if (h != 0f && v != 0f)
        {
            idle = true;
        }
        else
        {
            idle = false;
        }

        anim.SetFloat("horizontalSpeed", h);
        anim.SetFloat("verticalSpeed", v);
        anim.SetBool("isIdle", idle);

    }

    // Move player left & Right
    private void FixedUpdate()
    {
        rb.velocity = new Vector2(movement * movementSpeed, rb.velocity.y);
    }

    // Player jump logic
    private void Jump()
    {
        if (isGrounded)
        {
            rb.velocity = new Vector2(rb.velocity.x, jumpStrength);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            isGrounded = true;
            // Play idle animation
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            isGrounded = false;
        }
    }
}