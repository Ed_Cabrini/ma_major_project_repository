using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class anchorIKTargetGround : MonoBehaviour
{
    private RaycastHit2D hit;
    public LayerMask hitLayers;
    public Transform anchorTarget;

    // Get ray cast hit location and transform position to hit location
    void Update()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, -Vector2.up, 15f, hitLayers);
        {
            anchorTarget.position = hit.point;
        }
    }
}
