using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class playerController : MonoBehaviour
{
    // Movement variables
    public float movementSpeed;
    public float jumpStrength;
    public float stepDistance;
    public float stepHeight;
    public float footSpeed;
    public float footLiftSpeed;
    public float footJump;
    public float handJump;
    public float handSpeed;
    public float bodyFloorOffset;

    private float movement;
    private float distanceToTargetRight;
    private float distanceToTargetLeft;
    private bool isGrounded;
    private Rigidbody2D rb;
    private PlayerInputManager controls;

    // IK Object variables
    public GameObject rightFootSolver;
    public GameObject leftFootSolver;
    public GameObject rFwdStepTarget;
    public GameObject lFwdStepTarget;
   
    public GameObject characterRoot;
    
    public GameObject rightHandSolver;
    public GameObject leftHandSolver;
    public GameObject rightHandTarget;
    public GameObject leftHandTarget;

    // Player inventory objects
    public GameObject rightHandObj;
    public GameObject leftHandObj;

    // Get player rigidbody, assign new input, get movement context from input manager
    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        controls = new PlayerInputManager();
        controls.Player.Movement.performed += ctx => movement = ctx.ReadValue<float>();
        controls.Player.Movement.canceled += _ => movement = 0;

        controls.Player.Jump.started += _ => Jump();
    }

    private void OnEnable() => controls.Enable(); // Apply input
    private void OnDisable() => controls.Disable(); // Cancel input

    private void Update()
    {
        // Offset Root Y pos based on avaerage R&L foot step target Pos Y
        //float rFwdStepTargetPosY = rFwdStepTarget.transform.position.y;
       // float lFwdStepTargetPosY = lFwdStepTarget.transform.position.y;

        //characterRoot.transform.position = new Vector3(characterRoot.transform.position.x,((rFwdStepTargetPosY+lFwdStepTargetPosY)/2+bodyFloorOffset), characterRoot.transform.position.z);

        // Flip Player based on movement direction
        if (movement < 0)
            transform.localScale = new Vector3(1, -1, 1);
        else if (movement > 0)
            transform.localScale = new Vector3(1, 1, 1);

        // Check distance between Foot effector and step target
        if (rFwdStepTarget != null && rightFootSolver != null)
        {
            distanceToTargetRight = Vector2.Distance(rFwdStepTarget.transform.position, rightFootSolver.transform.position); // Right foot distance
        }
        if (lFwdStepTarget != null && leftFootSolver != null)
        {
            distanceToTargetLeft = Vector2.Distance(lFwdStepTarget.transform.position, leftFootSolver.transform.position); // Left foot distance
        }

        // Move feet to new step location
        if (isGrounded)
        {
            if (distanceToTargetRight > stepDistance && !leftFootMove.IsActive())
            {
                if (!rightFootMove.IsActive())
                    rightFootMove = rightFootSolver.transform.DOJump(rFwdStepTarget.transform.position, footJump, 1, footSpeed);
            }
            if (distanceToTargetLeft > stepDistance && !rightFootMove.IsActive())
            {
                if (!leftFootMove.IsActive())
                    leftFootMove = leftFootSolver.transform.DOJump(lFwdStepTarget.transform.position, footJump, 1, footSpeed);
            }
        }

        // Move hand target and make hand solver target follow if no object in hand
        if (isGrounded)
        {
            if (!leftHandMove.IsActive() && leftFootMove.IsActive() && rightHandObj == null)
            {
                if (!rightHandMove.IsActive())
                    rightHandMove = rightHandSolver.transform.DOJump(rightHandTarget.transform.position, handJump, 1, handSpeed);
            }
            if (!rightHandMove.IsActive() && rightFootMove.IsActive() && leftHandObj == null)
            {
                if (!leftHandMove.IsActive())
                    leftHandMove = leftHandSolver.transform.DOJump(leftHandTarget.transform.position, handJump, 1, handSpeed);
            }
        }
    }

    // Foot move variable
    Tween rightFootMove;
    Tween leftFootMove;

    // Hand move variable
    Tween rightHandMove;
    Tween leftHandMove;

    // Move player left & Right
    private void FixedUpdate()
    {
        rb.velocity = new Vector2(movement * movementSpeed, rb.velocity.y);
    }

    // Player jump logic
    private void Jump()
    {
        if (isGrounded)
        {
            rb.velocity = new Vector2(rb.velocity.x, jumpStrength);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            isGrounded = true;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            isGrounded = false;
        }
    }
}